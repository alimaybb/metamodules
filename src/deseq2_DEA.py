#!/usr/bin/env python

import sys
import os
import argparse

import OsTools as ot

def argParse():

    parser = argparse.ArgumentParser(description = 'How to run this script:')
    
    parser.add_argument('-c', '--counts', required = True,
                        help = 'The count file')
    parser.add_argument('-d', '--design_matrix', required = True,
                        help = 'The design matrix file')    
    parser.add_argument('-p', '--paired', action = 'store_true',
                        help = 'Whether the samples are paired or not')
    parser.add_argument('-prf', '--pre_filter', action = 'store_true', default = False,
                        help = 'Filter genes/KOs with low counts per million using edgeR before DESeq2 DEA (pre-filtering')
    parser.add_argument('-psf', '--post_filter', action = 'store_true', default = False,
                        help='Filter genes/KOs with low mean expression after DESeq2 differential expression analysis (post-filtering)')
    parser.add_argument('-r', '--replace_pvals', action = 'store_true', default = False,
                        help = 'Replace the P-values=0 with min P-value')
    
    args = parser.parse_args()
    return args

def main():

    args = argParse()    
    KO_count_file = os.path.abspath(args.counts)
    design_matrix_file = os.path.abspath(args.design_matrix)
    is_paired = args.paired
    do_pre_filtering = args.pre_filter
    do_post_filtering = args.post_filter
    replace_pvals = args.replace_pvals

    src_path = os.path.abspath(os.path.dirname(__file__))
    
    if is_paired:
        is_paired = 'TRUE'
    else:
        is_paired = 'FALSE'
    if do_post_filtering:
        do_post_filtering = 'TRUE'
    else:
        do_post_filtering = 'FALSE'
    if do_pre_filtering:
        do_pre_filtering = 'TRUE'
    else:
        do_pre_filtering = 'FALSE'
    if replace_pvals:
        replace = 'TRUE'
    else:
        replace = 'FALSE'   

    cmd =   'Rscript ' + src_path + '/DESeq2_DEA.R ' + KO_count_file + ' ' + design_matrix_file +  \
            ' ' + is_paired + ' ' + do_pre_filtering + ' ' + do_post_filtering + ' ' + replace
    
    ot.MakeSysCall(cmd)

if __name__ == '__main__':
    sys.exit(main())