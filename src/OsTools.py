import os
import sys
from subprocess import call

def MakeSysCall (cmd):
    returnCode = call(cmd, shell=True)
    if returnCode != 0:
        sys.stderr.write(cmd+" did not produce a return code of 0, quiting!\n") 
        sys.exit()

def CheckPath(path):
    if os.path.exists(path) == False:
        sys.stderr.write("The directory/file "+path+" does not exist! Quiting!\n")
        sys.exit()
             
def MakeDir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)
    else:
        sys.stderr.write("Note: the directory "+dir+" exists!\n")