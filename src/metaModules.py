#!/usr/bin/env python

import sys
import os
import platform
import re
import argparse
from os.path import *

import Env as env
import OsTools as ot

def parseHeinz (heinzOutput):
    
    heinzDict = {}
    reg = re.compile(r'\d+.*label="(.+)"')
    fh = open(heinzOutput, 'r')
    for line in fh:
        line = line.rstrip()
        m = re.search(reg, line)
        if m:
            match = m.group(1)
            linelist = match.split('\\n')
            id = linelist[0]
            score = linelist[1]
            heinzDict[id] = score
    fh.close()    
    return heinzDict

def getPathways (mainSetList, itemToPathwayList):
      
    itemsInPathwaysDict = {}
    pathwayDict = {}
    
    fhIN = open(mainSetList, 'r')
    lines = fhIN.readlines()[1:]
    fhIN.close()
    for line in lines:
        linelist = line.rstrip().split('\t')
        id = linelist[0]
        title = linelist[3]
        pathwayDict[id] = title
    
    fhIN = open(itemToPathwayList, 'r')
    for line in fhIN:
        linelist = line.rstrip().split()
        itemId = linelist[0]
        pathwaylist = linelist[1:]
        if itemId not in itemsInPathwaysDict:
            itemsInPathwaysDict[itemId] = set()
        for id in pathwaylist:
            itemsInPathwaysDict[itemId].add(id)
    fhIN.close()

    return pathwayDict, itemsInPathwaysDict

def generateNodesList (weight_dict, itemStatsDict, itemsInPathwaysDict, heinzDict, nodetype, gene_name_dict):
    
    fhout = open('nodes_induced.txt', 'w')
    pathwayNodeDict = {}
    
    fhout.write('#ID\tModule\tHeinzScore\tPvalue\tFoldChange\tSymbol\tURL\tPathway\n')
    
    for item in itemStatsDict:
        fhout.write(item + '\t')
        if item in heinzDict:
            fhout.write('MSS' + '\t' + str(weight_dict[item]) + '\t')
        else:
            fhout.write('\t\t')
        fhout.write(itemStatsDict[item]['pval'] + '\t' + itemStatsDict[item]['fc'] + '\t')
        if item in gene_name_dict:
            fhout.write(gene_name_dict[item] + '\t')
        else:
            fhout.write(item + '\t')
        if nodetype == 'gene':
            fhout.write('http://www.genome.jp/dbget-bin/www_bget?' + item + '\t')
        if nodetype == 'ko':
            fhout.write('http://www.genome.jp/dbget-bin/www_bget?ko:' + item + '\t')    
        elif nodetype == 'rn':
            fhout.write('http://www.genome.jp/dbget-bin/www_bget?rn:' + item + '\t')
        if item in heinzDict:
            l = itemsInPathwaysDict[item]
            pathways = '|'.join(l)
            fhout.write(pathways)
            for p in l:
                if p not in pathwayNodeDict:
                    pathwayNodeDict[p] = []
                pathwayNodeDict[p].append(item)
        fhout.write('\n')
    fhout.close()
        
    return pathwayNodeDict

def generateEdgeList (mainEdgeList, itemStatsDict):
    
    fhout = open('edges_induced.txt', 'w')
    fh = open(mainEdgeList, 'r')
    for line in fh:
        linelist = line.rstrip().split()
        f = linelist[0]
        s = linelist[1]
        if f in itemStatsDict and s in itemStatsDict:
            fhout.write(f + '\t' + s + '\n')
    fhout.close()
    fh.close()
    
def generateSetsList (pathwayDict, heinzPathwayDict, heinzDict):
    
    fhout = open('sets_induced.txt', 'w')
    fhout.write('ID\tCategory\tScore\tSymbol\tURL\n')
    for pathway in pathwayDict:
        if pathway in heinzPathwayDict:
            items = '+'.join(heinzPathwayDict[pathway])
            score = 0
            for item in heinzPathwayDict[pathway]:
                score += float(heinzDict[item])
            fhout.write(pathway + '\tKEGG\t' + str(score) + '\t' + pathwayDict[pathway] + \
                        '\thttp://www.genome.jp/kegg-bin/show_pathway?' + pathway + '+' + items + '\n')      
    fhout.close()

def parseTopTable (topTableFile, isgene = False):
    
    dict = {}
    fh = open(topTableFile, 'r')
    lines = fh.readlines()[1:] # Skip the header line
    fh.close()
    
    for line in lines:
        line = line.rstrip()
        l = line.split()
        id = l[0]
        if isgene == True:
            id = id.lower()
        basemean = l[1]
        foldchange = l[2]
        pval = l[5]

        if id not in dict:
            dict[id] = {}
            
        dict[id]['basemean'] = basemean
        dict[id]['fc'] = foldchange
        dict[id]['pval'] = pval 
    
    return dict

def runHeinz(weight_file, edge_file):
    
    heinz_cmd = env.heinzBin + ' -m 16'
    heinz_cmd += ' -n ' + weight_file 
    heinz_cmd += ' -e ' + edge_file + ' 1> heinz_stdout.txt 2> heinz_stderr.txt'
    ot.MakeSysCall(heinz_cmd)
    
    fhout = open('heinz_command.txt', 'w', 0)
    fhout.write(heinz_cmd)
    fhout.close()

def calculateWeights(a, l, fdr, pval_file, edge_file):
    
    print_out = 'weights_stdout_' + str(fdr) + '.txt'
    print_err =  'weights_' + str(fdr) + '.txt'
    
    print_cmd = env.heinzPrintBin + ' -a ' + a + ' -lambda ' + l +' -FDR ' + str(fdr) + ' ' \
                + pval_file + ' ' + edge_file + ' 1> ' + print_out + ' 2>' + print_err
    ot.MakeSysCall(print_cmd)
    
    weight_dict = {}
    p = platform.system()
    if p == 'Darwin':
        fi = open(print_out)
        for line in fi:
            line = line.rstrip()
            if 'label' in line:
                l = line.split('=\'')
                gene_id = l[1].split('\\n')[0]
                weight = l[1].split('\\n')[1].split('\\n')[0]
                weight_dict[gene_id] = weight    
    else:
        FINlist = open(print_err).readlines()[4:]
        for el in FINlist:
            t = el.rstrip().split()
            weight_dict[t[0]] = t[1]
    
    return weight_dict

def getBumParameters(BUM_parameter_file):

    lines = open(BUM_parameter_file, 'r').readlines()
    l = lines[0].rstrip()
    a = lines[1].rstrip()
    
    return l, a

def getGeneNames(gene_names):

    d = {}
    lines = open(gene_names, 'r').readlines()
    for line in lines:
        linelist = line.rstrip().split()
        id = linelist[0]
        name = linelist[1]
        d[id] = name
    return d

def writePvals (pval_file, item_stats_dict):
    
    fout = open(pval_file, 'w', 0)
    for el in item_stats_dict:
        if item_stats_dict[el]['pval'] == 'NA' or item_stats_dict[el]['pval'] == 1:
            item_stats_dict[el]['pval'] = 0.99
        fout.write(el + '\t' + str(item_stats_dict[el]['pval']) + '\n')
    fout.close()

def parseArgs():
    
    parser = argparse.ArgumentParser(description='Runs heinz with given input')
    parser.add_argument('-d', '--dea_result', required = True, type = str, 
                        help = 'The result file from deseq2 differential expression analysis')
    parser.add_argument('-b','--bum', required = True, type = str, 
                        help ='The bum parameter file')
    parser.add_argument('-e', '--edges', required = True, type = str,
                        help = 'The edge file')
    parser.add_argument('-p', '--pathways',
                        help = 'The pathway file')
    parser.add_argument('-s', '--sets', required = True, type = str,
                        help = 'The set file')
    parser.add_argument('-fdr', '--false_discovery_rate', required = True, type = float,
                        help = 'False discovery rate')
    parser.add_argument('-f', '--fc_threshold', type = float, 
                        help = 'Replace all weights with log2fc smaller than fc_threshold with infinitely small values')
    parser.add_argument('-i','--iters', default = 0, type = int,
                        help = 'The number of maximum-scoring subnetworks to be calculated')
    parser.add_argument('-n','--node_type', default='KO', choices = ['gene','ko', 'rn'], required = False,
                        help = 'Node type (KO/gene)')
    parser.add_argument('-g', '--node_name_file', required = False, type = str,
                        help = 'An optional tab delimited file with node IDs and names')
    args = parser.parse_args()
    
    return args

def main():

    # Parse input options        
    args = parseArgs()

    dea_results_file = os.path.abspath(args.dea_result)
    BUM_parameter_file = os.path.abspath(args.bum)
    edge_file = os.path.abspath(args.edges)
    pathway_file = os.path.abspath(args.pathways)
    set_file = os.path.abspath(args.sets)
    fdr = args.false_discovery_rate
    fc_threshold = args.fc_threshold
    nodetype = args.node_type
    nr_iter = args.iters
    gene_names = args.node_name_file

    # Parse if gene names are provided
    gene_name_dict =  {}
    if gene_names:
        gene_names = os.path.abspath(gene_names)
        gene_name_dict = getGeneNames(gene_names)    
    
    edge_file_no_ext = splitext(basename(edge_file))[0]
    heinzDirName = 'heinz_results'
    heinzPath = os.path.dirname(dea_results_file) + '/' + heinzDirName
    ot.MakeDir(heinzPath)
    os.chdir(heinzPath)

    # Parse stats (P-vals, fold changes etc) 
    item_stats_dict = parseTopTable(dea_results_file)

    # Parse BUM parameters        
    l, a = getBumParameters(BUM_parameter_file)
  
    pval_file = heinzPath + '/pvals.txt'
    writePvals(pval_file, item_stats_dict)
    
    fdr_dir = 'FDR_' + str(fdr) + '_' + edge_file_no_ext

  
        
    if fc_threshold:
        fdr_dir += '_fc_thrshld_' + str(fc_threshold)
    
    ot.MakeDir(fdr_dir)
    fdr_dir = os.path.abspath(fdr_dir)
    os.chdir(fdr_dir)
    # Calculate weights for genes/kos/reactions in the differential expression analysis result file
    weight_dict = calculateWeights(a, l, fdr, pval_file, edge_file)

    # If a fold change threshold is specified, take out all genes/KOs from the network
    # that has abs(logfc) < threshold
    if fc_threshold:
        for el in item_stats_dict:
            if abs(float(item_stats_dict[el]['fc'])) < fc_threshold:
                del weight_dict[el]
 
    iter_set = set()
    for i in range(1,nr_iter + 1):
        iter_folder = 'MSS_' + str(i)
        
        # If MSS_i folder exists for the ith maximum-scoring subnetwork
        # Add the list of MSS components to inter_set
        if os.path.exists(iter_folder):
            d = parseHeinz(iter_folder + '/heinz_stdout.txt')
            for el in d:
                iter_set.add(el)
            continue
        
        # Create the MSS_i folder and chdir
        ot.MakeDir(iter_folder)
        os.chdir(iter_folder)
        weight_file = 'MSS_' + str(i) + '_weights.txt'
        FOUT = open(weight_file, 'w', 0)
        for el in weight_dict:
            # If gene/ko/rn is not already in an MSS_j (j<i), write to the weight file for MSS_i
            if el not in iter_set:
                FOUT.write(el + '\t' + str(weight_dict[el]) + '\n')
        FOUT.close()
        
        # Run heinz on the network in edge_file with network weights in weight_file
        runHeinz(weight_file, edge_file)
 
        # Induce the set of edges for MSS_i fusing the main edge file
        generateEdgeList(edge_file, item_stats_dict)
        # Pathways
        pathway_dict, items_in_pathways_dict = getPathways(set_file, pathway_file)
        # Get stats of MSS_i components
        heinz_dict = parseHeinz('heinz_stdout.txt')
        # Generate a list of nodes
        heinz_pathway_dict = generateNodesList(weight_dict, item_stats_dict, items_in_pathways_dict, \
                                               heinz_dict, nodetype, gene_name_dict)
        # Generate the pathway list
        generateSetsList(pathway_dict, heinz_pathway_dict, heinz_dict)       
        # Print MSS_i components
        fhout = open('MSS_' + str(i) + '_components.txt', 'w', 0)
        for comp in heinz_dict:
            fhout.write(comp + '\n')
        fhout.close()
        for el in heinz_dict:
            iter_set.add(el)
        os.chdir(fdr_dir)
        
if __name__ == '__main__':
    main()