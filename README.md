## REQUIREMENTS ##

*     Python v2.7.3 or higher
*     R v3.1.1 or higher
*     R libraries DESeq2 (http://bioconductor.org/packages/release/bioc/html/DESeq2.html) and BioNet (http://www.bioconductor.org/packages/release/bioc/html/BioNet.html)
*     Heinz (https://software.cwi.nl/software/heinz/releases/2.0)

## SCRIPTS ##
### deseq2_DEA.py ###
deseq2_DEA.py is a simple python script that uses src/DESeq2_DEA.R. It takes two required arguments, a raw KO count matrix and a design matrix file describing the experimental setup. For the required input format of these files see example_KO_countx.txt, example_design_matrix_unpaired.txt and example_design_matrix_paired.txt in the examples/ directory.

deseq2_DEA.py performs the (rna-Seq) differential expression analysis steps that are described in the DESeq2 vignette (http://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.pdf). It offers two optional arguments that allow (i) the analysis of paired samples and (ii) the default DESeq2 filtering of genes (in this case KOs) with very low mean expression values. Using the BioNet R package, the script also fits a BUM model to the distribution of *P*-values (which are calculated from the input file) and outputs the result of the analysis as well as the BUM model parameters.

Note: If you already have performed differential expression analysis and wish to use the statistics from your own analysis, you can skip the deseq2_DEA.py to the calculation of BUM model parameters.

### fitBum.R ###
The input to fitBum.R is a file that contains a list of P-values. The script fits a BUM model to the distribution of these *P*-values and outputs the model parameters.

### metaModules.py ###
metaModules.py is the main script that runs Heinz to calculate the significantly deregulated maximum-scoring subnetworks. It also outputs the eXamine (http://apps.cytoscape.org/apps/examine) files that can be used to visualise calculated subnetworks.

## IMPORTANT NOTES ##

The location of the 'heinz' and 'print' binaries from the heinz package should be specified in src/Env.py

Default locations are in src/Env.py are:

heinzBin = home+'/packages/heinz/build/heinz'

heinzPrintBin = home+'/packages/heinz/build/print'

## USAGE ##

python deseq2_DEA.py -c [count_matrix] -d [design_matrix] -p [is_paired_analysis] -f [is_deseq2_independent_filtering]

python metaModules.py -d [dea_results.txt] -b [bum_model_parameter_file] -fdr [false_discovery_rate] -i [number_of_iterations] -nc [nr_of_cores]

## EXAMPLES ##

python src/deseq2_DEA.py -c example/example_KO_counts.txt -d example/example_design_matrix.txt -f

Rscript src/fitBum.R pvals.txt

python src/metaModules.py -d example/deseq2_fc_pval.txt -b example/deseq2_bumresults.txt -fdr 0.05

contact: Ali May (a.may@vu.nl)